# CSPC Front-End

My try on redesigning http://cspc.cs.upi.edu

## Development
This project use npm package. Make sure node.js and npm is installed.

### Installing Dependencies

```
npm install
```

### Compiling assets
This project use webpack and laravel-mix to manage assets.

To compile:

```
npm dev
```

To use auto-reloading with BrowserSync, create vhost named `cspc-fe.dev` for
this project or move it to your webroot directory and change `proxy` directive in
`webpack.mix.js` (e.g.: `proxy: 'localhost/cspc-fe'`). Then run:

```
npm watch
```
