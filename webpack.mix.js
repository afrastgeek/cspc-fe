let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('./');

mix.js('src/js/app.js', 'assets/js')
   .sass('src/sass/app.scss', 'assets/css')
   .options({ processCssUrls: false });

mix.browserSync({
    files: '*.html',
    proxy: 'http://localhost/cspc-fe/'
});
